#!/bin/env node
//  OpenShift sample Node application
var express = require('express');
var fs      = require('fs');
var connect = require('connect');

var app = express();
var port     = process.env.OPENSHIFT_NODEJS_PORT || 8080;
var server_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';


app.use(express.static(__dirname + '/public'));
app.use(connect.logger('dev')); 
app.use(connect.json()); 
app.use(connect.urlencoded());


require('./routes/routes.js')(app);

app.listen(port, server_address, function () {
    console.log('listening');
});

console.log("Check the app at Port : " + port);
